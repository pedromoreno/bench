LIB_PATH?=../lfht-hhl

CC=gcc
CFLAGS=-std=gnu11 -Wall -I$(LIB_PATH)
OPT=-O3 -ggdb #-flto
LFLAGS=-lpthread
DEBUG=-g -ggdb -Og -DDEBUG=1

default: bench
debug: bench_debug
all: bench bench_debug

$(LIB_PATH)/liblfht.a: $(LIB_PATH)/lfht.c $(LIB_PATH)/lfht.h
	cd $(LIB_PATH) && make liblfht.a

bench: bench.c $(LIB_PATH)/liblfht.a $(LIB_PATH)/lfht.h
	$(CC) bench.c $(LIB_PATH)/liblfht.a $(CFLAGS) $(OPT) $(LFLAGS) -o bench

$(LIB_PATH)/liblfht_debug.a: $(LIB_PATH)/lfht.c $(LIB_PATH)/lfht.h
	cd $(LIB_PATH) && make liblfht_debug.a

bench_debug: bench.c $(LIB_PATH)/liblfht_debug.a $(LIB_PATH)/lfht.h
	$(CC) bench.c $(LIB_PATH)/liblfht_debug.a $(CFLAGS) $(DEBUG) $(LFLAGS) -o bench_debug

clean:
	rm -f *.o bench bench_debug
	cd $(LIB_PATH) && make clean
